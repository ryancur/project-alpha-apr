from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from tasks.models import Task
from tasks.forms import TaskCreateForm


@login_required
def list_tasks(request):
    projects = Task.objects.filter(assignee=request.user)
    context = {"tasks_list": projects}
    return render(request, "tasks/tasks_list.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskCreateForm()
    context = {"task_create_form": form}
    return render(request, "tasks/create_task.html", context)
