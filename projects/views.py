from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from projects.models import Project
from projects.forms import ProjectCreateForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects_list": projects}
    return render(request, "projects/projects_list.html", context)


@login_required
def show_project(request, id=id):
    project = get_object_or_404(Project, id=id)
    context = {"project_detail": project}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectCreateForm()
    context = {"project_create_form": form}
    return render(request, "projects/create_project.html", context)
