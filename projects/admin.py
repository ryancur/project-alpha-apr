from django.contrib import admin
from projects.models import Project


@admin.register(Project)
class ProjectsAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "description", "owner")
